﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CronManage
{
    public partial class SQLCSB : Form
    {

        public string ret=null;
        public SQLCSB()
        {
            InitializeComponent();
           
        }

        public SQLCSB(string sqlcsb) : this()
        {
            this.ret = sqlcsb;
            SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();
            if (ret != "") scsb.ConnectionString = ret;
            this.propertyGrid1.SelectedObject = scsb;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.ret = this.propertyGrid1.SelectedObject.ToString();
            Close();
        }
    }
}
