﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;

namespace CronManage
{
    public partial class cronconfig : Form
    {
        public string retconndbstr;
        public cronconfig()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            config cfg = new config();
            cfg.connstring = textBox1.Text;
            cfg.host = textBox2.Text;
            cfg.port = textBox3.Text;
            cfg.mailFrom = textBox4.Text;
            cfg.mailPwd = textBox5.Text;
            cfg.EnableSSL = checkBox1.Checked;
            cfg.TestToMailAddress = buttonEdit3.Text;

            cfg.CreateXml("config.xml");

            retconndbstr = textBox1.Text;

            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void cronconfig_Shown(object sender, EventArgs e)
        {

            if (File.Exists("config.xml"))
            {
                XDocument xd = XDocument.Load("config.xml");

                foreach (XElement item in xd.Root.Descendants("mail"))
                {
                    if (item.Element("host") != null) textBox2.Text=item.Element("host").Value;
                    if (item.Element("port") != null) textBox3.Text = item.Element("port").Value;
                    if (item.Element("mailPwd") != null) textBox4.Text = item.Element("mailFrom").Value;
                    if (item.Element("mailPwd")!=null) textBox5.Text = item.Element("mailPwd").Value;
                    if (item.Element("EnableSSL") != null)     checkBox1.Checked = Convert.ToBoolean(item.Element("EnableSSL").Value);
                    if (item.Element("TestToMailAddress") != null) buttonEdit3.Text = item.Element("TestToMailAddress").Value;
                }

                foreach (XElement item in xd.Root.Descendants("conndb"))
                {
                    if (item.Element("connstring") != null) textBox1.Text = item.Element("connstring").Value;
                }
            }
            else
            {
                textBox1.Text = "server=192.168.18.18;database=test;uid=sa;pwd=123@qwe;MultipleActiveResultSets=true;Min Pool Size=1";
                textBox2.Text = "mail.hangyickpaper.cn";
                textBox3.Text = "30";
                textBox4.Text = "管理员<erp@hangyickpaper.cn>";
                checkBox1.Checked = false;
            }
        }

        private void buttonEdit3_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            mailAddressDialog f1 = new mailAddressDialog(buttonEdit3.Text);

            f1.ShowDialog();
            if (f1.DialogResult == DialogResult.OK)
            {
                buttonEdit3.Text = f1.ret;
            }
            f1.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SQLCSB f1 = new SQLCSB(textBox1.Text);

            f1.ShowDialog();
            if (f1.DialogResult == DialogResult.OK)
            {
                textBox1.Text = f1.ret;
            }
            f1.Dispose();
        }
    }
}
