﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace CronManage
{
    public partial class HistoryRecord : Form
    {
        public HistoryRecord()
        {
            InitializeComponent();
        }


        private void HistoryRecord_Load(object sender, EventArgs e)
        {
            dateEdit1.DateTime = DateTime.Now;
            dateEdit2.DateTime = DateTime.Now;

             SqlConnection conn = new SqlConnection(mailfrm.connstr);

            //SqlConnection conn = mailfrm.GetConnection();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select ID,B.SN,b.排程名称,B.执行命令,A.附件,A.发送时间");
            sb.AppendLine("from cronhistory A inner join crontab B on a.sn=b.SN");
            sb.AppendLine($"where convert(varchar,发送时间,23) between '{dateEdit1.DateTime.ToString("yyyy-MM-dd")}' and '{dateEdit2.DateTime.ToString("yyyy-MM-dd")}' ");

            SqlDataAdapter dam = new SqlDataAdapter(sb.ToString(), conn);
            dam.Fill(dataSet1, "cronhistory");

            this.bindingSource1.DataMember = "cronhistory";


        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  SqlConnection conn = new SqlConnection(mailfrm.connstr);
            SqlConnection conn = mailfrm.GetConnection();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("select ID,B.SN,b.排程名称,B.执行命令,A.附件,A.发送时间");
            sb.AppendLine("from cronhistory A inner join crontab B on a.sn=b.SN");
            sb.AppendLine($"where convert(varchar,发送时间,23) between '{dateEdit1.DateTime.ToString("yyyy-MM-dd")}' and '{dateEdit2.DateTime.ToString("yyyy-MM-dd")}' ");

            if (textEdit1.Text != "")
            {
                sb.AppendLine($" and B.SN like '{textEdit1.Text}%'");
            }

            SqlDataAdapter dam = new SqlDataAdapter(sb.ToString(), conn);
            dataSet1.Clear();
            dam.Fill(dataSet1, "cronhistory");
            this.bindingSource1.DataMember = "cronhistory";

        }

        private void 发送邮件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string runCommand = $"{Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)}\\{dataSet1.Tables[0].Rows[gridView1.FocusedRowHandle]["执行命令"].ToString()}";
            string cronId = $"{dataSet1.Tables[0].Rows[gridView1.FocusedRowHandle]["SN"].ToString()}";
            string sendId = $"{dataSet1.Tables[0].Rows[gridView1.FocusedRowHandle]["ID"].ToString()}";
            System.Diagnostics.Process.Start(runCommand,$" {cronId} -H {sendId}");
        }
    }
}
