﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace CronManage
{
    class config
    {
        public string connstring { get; set; }

        public string host { get; set; }
        public string port { get; set; }
        public string mailFrom { get; set; }
        public string mailPwd { get; set; }

        public bool EnableSSL { get; set; }

        public string TestToMailAddress { get; set; }

        public void CreateXml(string xmlfile)
        {
            if (File.Exists(xmlfile))
            {
                if (!Directory.Exists("bak")) Directory.CreateDirectory("bak");
                File.Move(xmlfile,$"bak\\{xmlfile}_{DateTime.Now.ToString("yyyyMMdd-HHmmss")}.bak");
            }

            //方法一 按照DOM方式添加
            //实例化XDocument对象 
            XDocument xdoc = new XDocument();
            //创建根节点
            XElement root = new XElement("config");
            //创建一级子节点
            XElement fchild = new XElement("conndb");
            fchild.Add(new XElement("connstring", connstring));
            root.Add(fchild);

            XElement fchild1 = new XElement("mail");
            fchild1.Add(new XElement("mailFrom", mailFrom));
            fchild1.Add(new XElement("mailPwd", mailPwd));
            fchild1.Add(new XElement("host", host));
            fchild1.Add(new XElement("port", port));
            fchild1.Add(new XElement("EnableSSL", EnableSSL));
            fchild1.Add(new XElement("TestToMailAddress", TestToMailAddress));

            root.Add(fchild1);

            xdoc.Add(root);

            //使用XML的保存会自动在xml文件开始添加：<?xml version="1.0" encoding="utf-8"?>


            xdoc.Save(xmlfile);

        }

    }
}
