﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CronManage
{

	public partial class cronTimeDialog : Form
	{


		private static List<string> decoder(string s)
		{
            try
            {
                List<string> s1 = new List<string>();
                List<string> s2 = new List<string>();
                List<string> ret = new List<string>();
                s1 = s.Split(',').ToList();
                foreach (var s3 in s1)
                {
                    s2 = s3.Split('-').ToList();
                    if (s2.Count > 1)
                    {
                        for (int i = Convert.ToInt32(s2[0]); i <= Convert.ToInt32(s2[1]); i++)
                        {
                            ret.Add(i.ToString());
                        }
                    }
                    else
                    {
                        ret.Add(s2[0].ToString());
                    }

                }
                s1 = null;
                s2 = null;
                return ret;
            }
            catch 
            {
                return null;
            }
		}

		private static int WeekToInt(string dayOfWeek)
		{
			if (dayOfWeek == "星期天") return 0;
			else if (dayOfWeek == "星期一") return 1;
			else if (dayOfWeek == "星期二") return 2;
			else if (dayOfWeek == "星期三") return 3;
			else if (dayOfWeek == "星期四") return 4;
			else if (dayOfWeek == "星期五") return 5;
			else if (dayOfWeek == "星期六") return 6;
			else return -1;
		}

		private static string IntToWeek(int dayOfWeek)
		{
			switch (dayOfWeek)
			{
				case 0:
					return "星期天";
				case 1:
					return "星期一";
				case 2:
					return "星期二";
				case 3:
					return "星期三";
				case 4:
					return "星期四";
				case 5:
					return "星期五";
				case 6:
					return "星期六";
				default:
					return null;
			}
		}


	private static void initform(string rettmp, int startValue, int endValue, CheckedListBox clist, TextEdit b1, string selectpage)
		{
			clist.Items.Clear();
			if (selectpage == "天")
			{
				for (int i = startValue; i <= endValue; i++)
				{
					clist.Items.Add(i.ToString());
				}
				clist.Items.Add("L");

				startValue--;
				endValue--;

			}
			else if (selectpage == "月")
			{
				for (int i = startValue; i <= endValue; i++)
				{
					clist.Items.Add(i.ToString());
				}
				startValue--;
				endValue--;
			}
			else if (selectpage == "周")
			{
				clist.ColumnWidth = 150;
				for (int i = startValue; i <= endValue; i++)
				{
					clist.Items.Add(IntToWeek(i));
				}
			}
			else
			{
				for (int i = startValue; i <= endValue; i++)
				{
					clist.Items.Add(i.ToString());
				}
			}

			if (rettmp == "")
			{
				for (int i = startValue; i <= endValue; i++)
				{
					clist.SetItemChecked(i, true);
				}
				return;
			}


            if (rettmp != null)
			{
				if (rettmp.IndexOf('/') > 0)
				{
					b1.Text = (rettmp.Split('/').ToList())[1].ToString();
					if ((rettmp.Split('/').ToList())[0].ToString() == "*")
					{
						for (int i = startValue; i <= endValue; i++)
						{
							clist.SetItemChecked(i, true);
						}
					}
					else
					{

						List<string> s1 = decoder((rettmp.Split('/').ToList())[0].ToString());
                        if (s1 == null) return;
						foreach (var s2 in s1)
						{
							if (selectpage == "周") clist.SetItemChecked(clist.Items.IndexOf(IntToWeek(Convert.ToInt32(s2))), true);
							else
								clist.SetItemChecked(clist.Items.IndexOf(s2), true);
						}

					}
				}
				else
				{

					if (rettmp.ToString() == "*")
					{
						for (int i = startValue; i <= endValue; i++)
						{
							clist.SetItemChecked(i, true);
						}
					}
					else
					{

						List<string> s1 = decoder(rettmp.ToString());
                        if (s1 == null) return;
                        foreach (var s2 in s1)
						{
							if (selectpage == "周") clist.SetItemChecked(clist.Items.IndexOf(IntToWeek(Convert.ToInt32(s2))), true);
							else
								clist.SetItemChecked(clist.Items.IndexOf(s2), true);
						}

					}

				}

			}
		}


		//字符串转换成规则字串
		public static string encoder(int[] array)
		{
			List<List<int>> s = new List<List<int>>();

			List<string> ret = new List<string>();

			Array.Sort(array);

			int j = 0;
			s.Add(new List<int>());
			for (int i = 0; i < array.Length; i++)
			{
				if (s[j].Count == 0)
				{
					s[j].Add(array[i]);
					continue;
				}

				if (array[i] - 1 == s[j][s[j].Count - 1])
				{
					s[j].Add(array[i]);
				}
				else
				{
					j++;
					s.Add(new List<int>());
					s[j].Add(array[i]);
				}
			}


			for (int i = 0; i < s.Count; i++)
			{
				if (s[i].Count > 2)
				{
					ret.Add(s[i][0].ToString() + "-" + s[i][s[i].Count - 1]);
				}
				else
				{
					ret.Add(string.Join(",", s[i].ToArray()));
				}
			}
			return string.Join(",", ret.ToArray());

		}

		private static string RetEncoder(CheckedListBox clist, TextEdit b1, string selectpage)
		{
			List<int> rettemp = new List<int>();
			string retstr = null;
			if (selectpage == "天")
			{
				bool monthLastday = false;
				if (clist.GetItemChecked(clist.Items.IndexOf("L"))) monthLastday = true;
				clist.Items.RemoveAt(31);

				for (int i = 0; i < clist.Items.Count; i++)
				{
					if (clist.GetItemChecked(i)) rettemp.Add(Convert.ToInt32(clist.Items[i]));
				}

				if (rettemp.Count == clist.Items.Count)
				{
					retstr = "*";
				}
				else
				{
					retstr = encoder(rettemp.ToArray());
					if (monthLastday) retstr = retstr + ",L";
				}
			}
			else
			{
				for (int i = 0; i < clist.Items.Count; i++)
				{
					if (clist.GetItemChecked(i))
					{
						if (selectpage == "周")
						{
							rettemp.Add(Convert.ToInt32(WeekToInt(clist.Items[i].ToString()).ToString()));
						}
						else
						{
							rettemp.Add(Convert.ToInt32(clist.Items[i].ToString()));
						}
					}
				}

				if (rettemp.Count == clist.Items.Count)
				{
					//全选输出*，代表所有的
					retstr = "*";
				}
				else
					retstr = encoder(rettemp.ToArray());
			}

			//如果一个都不选，则输出*，代表全选
			if (rettemp.Count == 0) retstr = "*";

			if (b1.Text != "") retstr = retstr + '/' + b1.Text;
			return retstr;
		}

		//全选
		private static void SelectAll(CheckedListBox clist)
		{
			for (int i = 0; i < clist.Items.Count; i++)
			{
				clist.SetItemChecked(i, true);
			}
		}

		//反选
		private static void SelectClear(CheckedListBox clist)
		{
			for (int i = 0; i < clist.Items.Count; i++)
			{
				clist.SetItemChecked(i, false);
			}
		}


		public string ret;
		private string page;
		public cronTimeDialog()
		{
			InitializeComponent();
		}

		public cronTimeDialog(string dh, string tabpage)
		{
			if (dh != null) ret = dh;
			this.page = tabpage;
			InitializeComponent();
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (this.page == "分")
			{
				this.ret = RetEncoder(checkedListBox1, textEdit1, this.page);
			}
			else if (this.page == "时")
			{
				this.ret = RetEncoder(checkedListBox2, textEdit2, this.page);
			}
			else if (this.page == "天")
			{
				this.ret = RetEncoder(checkedListBox3, textEdit3, this.page);
			}
			else if (this.page == "月")
			{
				this.ret = RetEncoder(checkedListBox4, textEdit4, this.page);
			}
			else if (this.page == "周")
			{
				this.ret = RetEncoder(checkedListBox5, textEdit5, this.page);
			}

			this.DialogResult = DialogResult.OK;
			this.Close();

		}

		private void cronTimeDialog_Shown(object sender, EventArgs e)
		{
			this.tabControl1.Region = new Region(new RectangleF(this.tabPage1.Left, this.tabPage1.Top, this.tabPage1.Width, this.tabPage1.Height));

			if (this.page == "分")
			{
				this.tabControl1.SelectedTab = this.tabPage1;
				initform(ret, 0, 59, checkedListBox1, textEdit1, this.page);

			}

			if (this.page == "时")
			{
				this.tabControl1.SelectedTab = this.tabPage2;
				initform(ret, 0, 23, checkedListBox2, textEdit2, this.page);

			}

			if (this.page == "天")
			{
				this.tabControl1.SelectedTab = this.tabPage3;
				initform(ret, 1, 31, checkedListBox3, textEdit3, this.page);

			}

			if (this.page == "月")
			{
				this.tabControl1.SelectedTab = this.tabPage4;
				initform(ret, 1, 12, checkedListBox4, textEdit4, this.page);
			}

			if (this.page == "周")
			{
				this.tabControl1.SelectedTab = this.tabPage5;
				initform(ret, 0, 6, checkedListBox5, textEdit5, this.page);
			}

		}

		private void button3_Click(object sender, EventArgs e)
		{			
			SelectAll(checkedListBox1);
		}

		private void button4_Click(object sender, EventArgs e)
		{
			SelectClear(checkedListBox1);
		}

		private void button6_Click(object sender, EventArgs e)
		{
			SelectAll(checkedListBox2);
		}

		private void button5_Click(object sender, EventArgs e)
		{
			SelectClear(checkedListBox2);
		}

		private void button8_Click(object sender, EventArgs e)
		{
			for (int i=0; i < checkedListBox3.Items.Count - 1; i++)
			{
				checkedListBox3.SetItemChecked(i, true);
			}
		}

		private void button7_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < checkedListBox3.Items.Count; i++)
			{
				checkedListBox3.SetItemChecked(i, false);
			}
		}

		private void button9_Click(object sender, EventArgs e)
		{
			SelectClear(checkedListBox4);
		}

		private void button11_Click(object sender, EventArgs e)
		{
			SelectClear(checkedListBox5);
		}

		private void button10_Click(object sender, EventArgs e)
		{
			SelectAll(checkedListBox4);
		}

		private void button12_Click(object sender, EventArgs e)
		{
			SelectAll(checkedListBox5);
		}
	}
}
