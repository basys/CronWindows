﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CronManage
{
    public partial class cronjob : Form
    {
		private string jobid; //任务ID

        public cronjob()
        {
            this.jobid = null;
            InitializeComponent();
        }

        public cronjob(string jobid)
        {
            this.jobid = jobid;
            this.InitializeComponent();

        }


        private void cronjob_Load(object sender, EventArgs e)
        {
            string crontab, cronExportFile;



            if (this.jobid == null)
            {
                crontab = "select * from crontab where 0=1 ";
                cronExportFile = "select * from cronExportFile where 0=1 ";
            }
            else
            {
                crontab = string.Format("select * from crontab where SN='{0}'", jobid);
                cronExportFile = string.Format("select * from cronExportFile where SN='{0}'", jobid);
            }

            conn = new SqlConnection(mailfrm.connstr);
            dam = new SqlDataAdapter(crontab, conn);
            dad = new SqlDataAdapter(cronExportFile, conn);

            dam.Fill(dataSet1, "crontab");
            dad.Fill(dataSet1, "cronExportFile");

          
            new SqlCommandBuilder(dam);
            new SqlCommandBuilder(dad);

            if (this.jobid == null)
            {
                DataRow dr1 = dataSet1.Tables[0].NewRow();
                // dr1["SN"] = "job";
                dr1["MailTO"] = "basys@hangyickpaper.cn";
                dr1["分"] = "*";
                dr1["时"] = "*";
                dr1["天"] = "*";
                dr1["月"] = "*";
                dr1["周"] = "*";
                dr1["启用"] = true;
                dr1["执行命令"] = "cronsend.exe";
                dr1["附件格式"]= "xls";
                dr1["文件名后缀"]= "_yyyyMMdd";

                dr1["邮件发送失败重试"] = 0;
                dr1["重试间隔分钟"] = 30;
                dr1["失败重试次数"] = 3;

                dr1["附件HTML内容"] = 0;

                dr1["发送邮件类型"] = 0;
                dr1["发送钉钉消息"] = 0;

                dataSet1.Tables[0].Rows.Add(dr1);

                //单身增加空行
                DataRow row = this.dataSet1.Tables["cronExportFile"].NewRow();
                row["序号"] = 1;
                this.dataSet1.Tables["cronExportFile"].Rows.Add(row);
            }


            this.textBox1.DataBindings.Add("Text", dataSet1.Tables["crontab"], "SN", true);
            this.textBox2.DataBindings.Add("Text", dataSet1.Tables["crontab"], "排程名称", true);

            this.checkBox1.DataBindings.Add("checked", dataSet1.Tables["crontab"], "启用", true);

            this.buttonEdit1.DataBindings.Add("Text", dataSet1.Tables["crontab"], "MailTO");
            this.buttonEdit2.DataBindings.Add("Text", dataSet1.Tables["crontab"], "MailCC");
            this.buttonEdit8.DataBindings.Add("Text", dataSet1.Tables["crontab"], "MailBCC");

            this.buttonEdit3.DataBindings.Add("Text", dataSet1.Tables["crontab"], "分");
            this.buttonEdit4.DataBindings.Add("Text", dataSet1.Tables["crontab"], "时");
            this.buttonEdit5.DataBindings.Add("Text", dataSet1.Tables["crontab"], "天");
            this.buttonEdit6.DataBindings.Add("Text", dataSet1.Tables["crontab"], "月");
            this.buttonEdit7.DataBindings.Add("Text", dataSet1.Tables["crontab"], "周");
            this.buttonEdit9.DataBindings.Add("Text", dataSet1.Tables["crontab"], "数据栏位格式化");

            this.textBox5.DataBindings.Add("Text", dataSet1.Tables["crontab"], "MailSubject");
            this.textBox6.DataBindings.Add("Text", dataSet1.Tables["crontab"], "MailBody");
            this.textBox4.DataBindings.Add("Text", dataSet1.Tables["crontab"], "附件文件名");
            this.textBox3.DataBindings.Add("Text", dataSet1.Tables["crontab"], "执行命令");

            this.comboBox1.DataBindings.Add("Text", dataSet1.Tables["crontab"], "附件格式");
			this.comboBox2.DataBindings.Add("Text", dataSet1.Tables["crontab"], "文件名后缀");

            this.checkBox2.DataBindings.Add("checked", dataSet1.Tables["crontab"], "邮件发送失败重试");
            this.comboBox3.DataBindings.Add("Text", dataSet1.Tables["crontab"], "重试间隔分钟");
            this.spinEdit1.DataBindings.Add("Value", dataSet1.Tables["crontab"], "失败重试次数");
            
            this.checkBox3.DataBindings.Add("checked", dataSet1.Tables["crontab"], "附件HTML内容");

            this.radioGroup1.DataBindings.Add("EditValue", dataSet1.Tables["crontab"], "发送邮件类型");
            this.checkBox4.DataBindings.Add("checked", dataSet1.Tables["crontab"], "发送钉钉消息");
            this.textBox7.DataBindings.Add("Text", dataSet1.Tables["crontab"], "钉钉消息地址");

            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.DataSource = dataSet1;
            this.dataGridView1.DataMember = "cronExportFile";
            
            this.dataGridView1.Columns["SN"].DataPropertyName = "SN";
            this.dataGridView1.Columns["SN"].Visible = false;
            this.dataGridView1.Columns["序号"].DataPropertyName = "序号";
            
            this.dataGridView1.Columns["SheetName"].DataPropertyName = "SheetName";
            this.dataGridView1.Columns["存储过程"].DataPropertyName = "存储过程";
            this.dataGridView1.Columns["参数1"].DataPropertyName = "参数1";
            this.dataGridView1.Columns["参数2"].DataPropertyName = "参数2";
            this.dataGridView1.Columns["参数3"].DataPropertyName = "参数3";
            this.dataGridView1.Columns["参数4"].DataPropertyName = "参数4";
            this.dataGridView1.Columns["参数5"].DataPropertyName = "参数5";
            // dataSet1.Tables[1].Columns["序号"].AutoIncrement = true;
            // dataSet1.Tables[1].Columns["序号"].AutoIncrementStep = 1;
            // dataSet1.Tables[1].Columns["序号"].AutoIncrementSeed = 1;
            if (this.dataGridView1.Rows.Count > 0)
            {
                this.dataGridView1.CurrentCell = this.dataGridView1.Rows[0].Cells[2];
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void button1_Click(object sender, EventArgs e)
        {
			if (textBox2.Text == "")
			{
				MessageBox.Show("排程名称不能为空");
				return;
			}

			if (textBox3.Text == "")
			{
				MessageBox.Show("执行命令不能为空");
				return;
			}

			if (buttonEdit1.Text == "")
			{
				MessageBox.Show("邮件<MailTo>收件人不能为空");
				return;
			}

			if (textBox5.Text == "")
			{
				MessageBox.Show("邮件<Subject>主旨不能为空");
				return;
			}

			if (textBox6.Text == "")
			{
				MessageBox.Show("邮件内客<Body>不能为空");
				return;
			}

            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit3.Text, "^[0-9,*/-]*$"))
            {
                MessageBox.Show("分排程时间设有误,请重选");
                return;
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit4.Text, "^[0-9,*/-]*$"))
            {
                MessageBox.Show("时排程时间设有误,请重选");
                return;
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit5.Text, "^[0-9L,*/-]*$"))
            {
                MessageBox.Show("日排程时间设有误,请重选");
                return;
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit6.Text, "^[0-9,*-]*$"))
            {
                MessageBox.Show("月排程时间设有误,请重选");
                return;
            }

            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit7.Text, "^[0-6,*-]*$"))
            {
                MessageBox.Show("周排程时间设有误,请重选");
                return;
            }

            if (jobid == null)
            {
                string sn = GetSN(conn);
                this.dataSet1.Tables["crontab"].Rows[0]["SN"] = sn;

                for (int i = 0; i < this.dataSet1.Tables["cronExportFile"].Rows.Count; i++)
                {
                    this.dataSet1.Tables["cronExportFile"].Rows[i]["SN"] = sn;
                  //  this.dataSet1.Tables["cronExportFile"].Rows[i]["序号"] = i+1;
                }

            }
            else
            {
                string sn = this.textBox1.Text;
                for (int i = 0; i < this.dataSet1.Tables["cronExportFile"].Rows.Count; i++)
                {
                    this.dataSet1.Tables["cronExportFile"].Rows[i]["SN"] = sn;
                  //  this.dataSet1.Tables["cronExportFile"].Rows[i]["序号"] = i+1;
                }

            }

            this.BindingContext[this.dataSet1.Tables["crontab"]].EndCurrentEdit();
            this.BindingContext[this.dataSet1.Tables["cronExportFile"]].EndCurrentEdit();

            dam.Update(dataSet1.Tables["crontab"]);
            dad.Update(dataSet1.Tables["cronExportFile"]);

            this.DialogResult = DialogResult.OK;
            this.Close();
        }


        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            mailAddressDialog f1 = new mailAddressDialog(buttonEdit1.Text);
            
            f1.ShowDialog();
            if (f1.DialogResult == DialogResult.OK)
            {
                buttonEdit1.Text = f1.ret;
            }
            f1.Dispose();
        }

        private void buttonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            mailAddressDialog f1 = new mailAddressDialog(buttonEdit2.Text);

            f1.ShowDialog();
            if (f1.DialogResult == DialogResult.OK)
            {
                buttonEdit2.Text = f1.ret;
            }
            f1.Dispose();
        }

        private void buttonEdit8_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            mailAddressDialog f1 = new mailAddressDialog(buttonEdit8.Text);

            f1.ShowDialog();
            if (f1.DialogResult == DialogResult.OK)
            {
                buttonEdit8.Text = f1.ret;
            }
            f1.Dispose();
        }


        private void buttonEdit3_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit3.Text, "^[0-9,*/-]*$"))
            {
                MessageBox.Show("分排程时间设有误,请重选");
                return;
            }
            cronTimeDialog f1= new cronTimeDialog(buttonEdit3.Text, "分");
            if (f1.ShowDialog() == DialogResult.OK)
            {
                buttonEdit3.Text = f1.ret;
            }
            f1.Dispose();
        }

        private void buttonEdit4_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit4.Text, "^[0-9,*/-]*$"))
            {
                MessageBox.Show("时排程时间设有误,请重选");
                return;
            }
            cronTimeDialog f1 = new cronTimeDialog(buttonEdit4.Text, "时");
            if (f1.ShowDialog() == DialogResult.OK)
            {
                buttonEdit4.Text = f1.ret;
            }
            f1.Dispose();
        }

        private void buttonEdit5_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit5.Text, "^[0-9L,*/-]*$"))
            {
                MessageBox.Show("日排程时间设有误,请重选");
                return;
            }
            cronTimeDialog f1 = new cronTimeDialog(buttonEdit5.Text, "天");
            if (f1.ShowDialog() == DialogResult.OK)
            {
                buttonEdit5.Text = f1.ret;
            }
            f1.Dispose();
        }

        private void buttonEdit6_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit6.Text, "^[0-9,*-]*$"))
            {
                MessageBox.Show("月排程时间设有误,请重选");
                return;
            }

            cronTimeDialog f1 = new cronTimeDialog(buttonEdit6.Text, "月");
            if (f1.ShowDialog() == DialogResult.OK)
            {
                buttonEdit6.Text = f1.ret;
            }
            f1.Dispose();
        }

        private void buttonEdit7_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(buttonEdit7.Text, "^[0-6,*-]*$"))
            {
                MessageBox.Show("周排程时间设有误,请重选");
                return;
            }
            cronTimeDialog f1 = new cronTimeDialog(buttonEdit7.Text, "周");
            if (f1.ShowDialog() == DialogResult.OK)
            {
                buttonEdit7.Text = f1.ret;
            }
            f1.Dispose();
        }

        private static string GetSN(SqlConnection conn)
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select ISNULL('JOB'+RIGHT(RIGHT(MAX(SN),3)+1001,3),'JOB001') SN from crontab", conn);
            return cmd.ExecuteScalar().ToString();
        }

        private void dataGridView1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyValue == 40 ) 
            {
 

                if ((sender as DataGridView).Rows.Count == 0)
                {
                    DataRow row = this.dataSet1.Tables["cronExportFile"].NewRow();
                    row["序号"] = 1;
                    this.dataSet1.Tables["cronExportFile"].Rows.Add(row);
                    return;
                }

                if ((sender as DataGridView).CurrentRow.Index == (sender as DataGridView).Rows.Count - 1)
                {

                    DataRow row = this.dataSet1.Tables["cronExportFile"].NewRow();
                    row["序号"] = (sender as DataGridView).CurrentRow.Index + 2;
                    this.dataSet1.Tables["cronExportFile"].Rows.Add(row);
                }
            }
        }

        private void dataGridView1_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            if ((sender as DataGridView).Rows.Count == 0) return;

            for (int i = 0; i < this.dataSet1.Tables["cronExportFile"].Rows.Count; i++)
            {
                if ((int)this.dataSet1.Tables["cronExportFile"].Rows[i]["序号"] != i+1 )
                this.dataSet1.Tables["cronExportFile"].Rows[i]["序号"] = i + 1;
            }
        }

        private void buttonEdit9_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            mailAddressDialog f1 = new mailAddressDialog(buttonEdit9.Text);

            f1.ShowDialog();
            if (f1.DialogResult == DialogResult.OK)
            {
                buttonEdit9.Text = f1.ret;
            }
            f1.Dispose();
        }
    }
}
