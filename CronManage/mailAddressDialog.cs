﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CronManage
{
    public partial class mailAddressDialog : Form
    {
        public string ret;

        public mailAddressDialog()
        {
            InitializeComponent();
            ret = null;
        }

        public mailAddressDialog(string dh)
        {
            if (dh != null) ret = dh;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ret = string.Join(";", memoEdit1.Lines.ToArray());
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void mailAddressDialog_Shown(object sender, EventArgs e)
        {
            if (ret != null)
            {
                if (ret.IndexOf(',') > 0) ret.Replace(',', ';');
                memoEdit1.Lines = ret.Split(';');
            }
        }
    }
}
