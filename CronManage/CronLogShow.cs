﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CronCommon;
using System.IO;

namespace CronManage
{
    public partial class CronLogShow : Form
    {
        public string cronid=null;
        public CronLogShow()
        {           
            InitializeComponent();
            dateEdit1.DateTime = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void CronLogShow_Load(object sender, EventArgs e)
        {
            List<DirectoryList> dl=new List<DirectoryList>();
            

            foreach (var s in Dir.GetLogFileList(cronid))
            {
                
                dl.Insert(0,new DirectoryList(s));
            }
            if (dl.Count>0)
            {
                dl.OrderByDescending(x => x.filename);
                
                BindingSource a = new BindingSource();
                a.DataSource = dl;
                listBox1.DataSource = a;
                listBox1.DisplayMember = "filename";
                listBox1.ValueMember = "pathfilename";
                listBox1.SelectedIndex = 0;
                listBox1_Click(sender, e);
            }
            
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            //FileStream fs = new FileStream(listBox1.SelectedValue.ToString(), FileMode.Open, FileAccess.Read);
            //StreamReader sr = new StreamReader(fs, Encoding.UTF8);//注意第二个参数
            //richTextBox1.Text = sr.ReadToEnd();
            //sr.Close();
            //fs.Close();
            richTextBox1.Text = File.ReadAllText(listBox1.SelectedValue.ToString(), Encoding.UTF8);

            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dateEdit1.Text == "")
            {
                MessageBox.Show("需要查询的日期不要为空");
                return;
            }

            List<DirectoryList> dl = new List<DirectoryList>();

            foreach (var s in Dir.GetLogFileList(dateEdit1.DateTime))
            {
                String AllText = File.ReadAllText(s);

                if (AllText.Contains("邮件发送完成")) continue;
                if (!AllText.Contains("超时时间")) continue;
                dl.Insert(0, new DirectoryList(s));
            }

            if (dl.Count > 0)
            {
                dl.OrderByDescending(x => x.filename);

                BindingSource a = new BindingSource();
                a.DataSource = dl;
                listBox1.DataSource = a;
                listBox1.DisplayMember = "filename";
                listBox1.ValueMember = "pathfilename";
                listBox1.SelectedIndex = 0;
                listBox1_Click(sender, e);
            }

        }
    }



    public class DirectoryList
    {
        /// <summary>
        /// 文件名
        /// </summary>
        public string filename { get; set; }
        /// <summary>
        /// 完整绝对路径文件名
        /// </summary>
        public string pathfilename { get; set; }

        public DirectoryList(string pathfilename)
        {
            this.filename = Path.GetFileNameWithoutExtension(pathfilename);
            this.pathfilename = pathfilename;
        }

    }
}
