﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CronManage
{
    public partial class ServerInstall : Form
    {
        public ServerInstall()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (Services.IsServiceExisted(Services.serviceName)) Services.UninstallService(Services.serviceName);
            Services.InstallService(Services.serviceFilePath);
            toolStripStatusLabel2.Text = Services.ServiceStatus(Services.serviceName);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Services.IsServiceExisted(Services.serviceName))
            {
                Services.ServiceStart(Services.serviceName);
                toolStripStatusLabel2.Text = Services.ServiceStatus(Services.serviceName);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Services.IsServiceExisted(Services.serviceName))
            {
                Services.ServiceStop(Services.serviceName);
                toolStripStatusLabel2.Text = Services.ServiceStatus(Services.serviceName);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Services.IsServiceExisted(Services.serviceName))
            {
                Services.ServiceStop(Services.serviceName);
                Services.UninstallService(Services.serviceFilePath);
                toolStripStatusLabel2.Text = Services.ServiceStatus(Services.serviceName);
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = Services.ServiceStatus(Services.serviceName);
        }

    }
}
