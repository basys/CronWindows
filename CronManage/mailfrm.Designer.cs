﻿namespace CronManage
{
    partial class mailfrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mailfrm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton9 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton8 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton10 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton11 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.立即执行 = new System.Windows.Forms.ToolStripMenuItem();
            this.发送测试邮件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.日志查看ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ds = new System.Data.DataSet();
            this.SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.启用 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.排程名称 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.分 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.时 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.天 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.月 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.周 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.执行命令 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MailTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MailCC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MailBCC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MailSubject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MailBody = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.附件文件名 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.邮件发送失败重试 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.失败重试次数 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.重试间隔分钟 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.最后执行时间 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3,
            this.toolStripButton9,
            this.toolStripButton4,
            this.toolStripButton5,
            this.toolStripButton7,
            this.toolStripButton8,
            this.toolStripButton10,
            this.toolStripButton11,
            this.toolStripButton6});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1445, 56);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Image = global::CronManage.Properties.Resources.服务;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton1.Text = "服务管理";
            this.toolStripButton1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = global::CronManage.Properties.Resources.刷新应用;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton2.Text = "应用排程";
            this.toolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.Image = global::CronManage.Properties.Resources.新增;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton3.Text = "新增任务";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripButton9
            // 
            this.toolStripButton9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton9.Image")));
            this.toolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton9.Name = "toolStripButton9";
            this.toolStripButton9.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton9.Text = "修改任务";
            this.toolStripButton9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton9.Click += new System.EventHandler(this.toolStripButton9_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.Image = global::CronManage.Properties.Resources.删除任务;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton4.Text = "删除任务";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.Image = global::CronManage.Properties.Resources.ooopic_1553303386;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(36, 53);
            this.toolStripButton5.Text = "设置";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.Image = global::CronManage.Properties.Resources.启动;
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton7.Text = "启动服务";
            this.toolStripButton7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripButton8
            // 
            this.toolStripButton8.Image = global::CronManage.Properties.Resources.停止;
            this.toolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton8.Name = "toolStripButton8";
            this.toolStripButton8.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton8.Text = "停止服务";
            this.toolStripButton8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton8.Click += new System.EventHandler(this.toolStripButton8_Click);
            // 
            // toolStripButton10
            // 
            this.toolStripButton10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton10.Image")));
            this.toolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton10.Name = "toolStripButton10";
            this.toolStripButton10.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton10.Text = "发送历史";
            this.toolStripButton10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton10.Click += new System.EventHandler(this.toolStripButton10_Click);
            // 
            // toolStripButton11
            // 
            this.toolStripButton11.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton11.Image")));
            this.toolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton11.Name = "toolStripButton11";
            this.toolStripButton11.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton11.Text = "日志查看";
            this.toolStripButton11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton11.Click += new System.EventHandler(this.toolStripButton11_Click);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.Image = global::CronManage.Properties.Resources.RibbonPageData_tbExit;
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(60, 53);
            this.toolStripButton6.Text = "退出系统";
            this.toolStripButton6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 56);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1445, 792);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.statusStrip1);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1437, 766);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "任务清单";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(3, 741);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1431, 22);
            this.statusStrip1.Stretch = false;
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(56, 17);
            this.toolStripStatusLabel1.Text = "服务状态";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(0, 17);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SN,
            this.启用,
            this.排程名称,
            this.分,
            this.时,
            this.天,
            this.月,
            this.周,
            this.执行命令,
            this.MailTO,
            this.MailCC,
            this.MailBCC,
            this.MailSubject,
            this.MailBody,
            this.附件文件名,
            this.邮件发送失败重试,
            this.失败重试次数,
            this.重试间隔分钟,
            this.最后执行时间});
            this.dataGridView1.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1431, 735);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.DoubleClick += new System.EventHandler(this.dataGridView1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripSeparator2,
            this.立即执行,
            this.发送测试邮件ToolStripMenuItem,
            this.日志查看ToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(149, 148);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem3.Text = "修改任务";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(145, 6);
            // 
            // 立即执行
            // 
            this.立即执行.Name = "立即执行";
            this.立即执行.Size = new System.Drawing.Size(148, 22);
            this.立即执行.Text = "立即执行";
            this.立即执行.Click += new System.EventHandler(this.立即执行_Click);
            // 
            // 发送测试邮件ToolStripMenuItem
            // 
            this.发送测试邮件ToolStripMenuItem.Name = "发送测试邮件ToolStripMenuItem";
            this.发送测试邮件ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.发送测试邮件ToolStripMenuItem.Text = "发送测试邮件";
            this.发送测试邮件ToolStripMenuItem.Click += new System.EventHandler(this.发送测试邮件ToolStripMenuItem_Click);
            // 
            // 日志查看ToolStripMenuItem
            // 
            this.日志查看ToolStripMenuItem.Name = "日志查看ToolStripMenuItem";
            this.日志查看ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.日志查看ToolStripMenuItem.Text = "日志查看";
            this.日志查看ToolStripMenuItem.Click += new System.EventHandler(this.日志查看ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(145, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem1.Text = "启用任务";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(148, 22);
            this.toolStripMenuItem2.Text = "停用任务";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            // 
            // SN
            // 
            this.SN.HeaderText = "SN";
            this.SN.Name = "SN";
            this.SN.ReadOnly = true;
            this.SN.ToolTipText = "11";
            this.SN.Width = 45;
            // 
            // 启用
            // 
            this.启用.HeaderText = "启用";
            this.启用.Name = "启用";
            this.启用.ReadOnly = true;
            this.启用.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.启用.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.启用.Width = 35;
            // 
            // 排程名称
            // 
            this.排程名称.HeaderText = "排程名称";
            this.排程名称.Name = "排程名称";
            this.排程名称.ReadOnly = true;
            this.排程名称.Width = 170;
            // 
            // 分
            // 
            this.分.HeaderText = "分";
            this.分.Name = "分";
            this.分.ReadOnly = true;
            this.分.Width = 40;
            // 
            // 时
            // 
            this.时.HeaderText = "时";
            this.时.Name = "时";
            this.时.ReadOnly = true;
            this.时.Width = 40;
            // 
            // 天
            // 
            this.天.HeaderText = "天";
            this.天.Name = "天";
            this.天.ReadOnly = true;
            this.天.Width = 40;
            // 
            // 月
            // 
            this.月.HeaderText = "月";
            this.月.Name = "月";
            this.月.ReadOnly = true;
            this.月.Width = 40;
            // 
            // 周
            // 
            this.周.HeaderText = "周";
            this.周.Name = "周";
            this.周.ReadOnly = true;
            this.周.Width = 40;
            // 
            // 执行命令
            // 
            this.执行命令.HeaderText = "执行命令";
            this.执行命令.Name = "执行命令";
            this.执行命令.ReadOnly = true;
            this.执行命令.Width = 80;
            // 
            // MailTO
            // 
            this.MailTO.HeaderText = "MailTO";
            this.MailTO.Name = "MailTO";
            this.MailTO.ReadOnly = true;
            this.MailTO.Width = 240;
            // 
            // MailCC
            // 
            this.MailCC.HeaderText = "MailCC";
            this.MailCC.Name = "MailCC";
            this.MailCC.ReadOnly = true;
            this.MailCC.Width = 240;
            // 
            // MailBCC
            // 
            this.MailBCC.HeaderText = "MailBCC";
            this.MailBCC.Name = "MailBCC";
            this.MailBCC.ReadOnly = true;
            this.MailBCC.Width = 240;
            // 
            // MailSubject
            // 
            this.MailSubject.HeaderText = "MailSubject";
            this.MailSubject.Name = "MailSubject";
            this.MailSubject.ReadOnly = true;
            this.MailSubject.Width = 200;
            // 
            // MailBody
            // 
            this.MailBody.HeaderText = "MailBody";
            this.MailBody.Name = "MailBody";
            this.MailBody.ReadOnly = true;
            this.MailBody.Width = 240;
            // 
            // 附件文件名
            // 
            this.附件文件名.HeaderText = "附件文件名";
            this.附件文件名.Name = "附件文件名";
            this.附件文件名.ReadOnly = true;
            // 
            // 邮件发送失败重试
            // 
            this.邮件发送失败重试.HeaderText = "发送失败重试";
            this.邮件发送失败重试.Name = "邮件发送失败重试";
            this.邮件发送失败重试.ReadOnly = true;
            this.邮件发送失败重试.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.邮件发送失败重试.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.邮件发送失败重试.Width = 75;
            // 
            // 失败重试次数
            // 
            this.失败重试次数.HeaderText = "失败重试次数";
            this.失败重试次数.Name = "失败重试次数";
            this.失败重试次数.ReadOnly = true;
            this.失败重试次数.Width = 75;
            // 
            // 重试间隔分钟
            // 
            this.重试间隔分钟.HeaderText = "重试间隔分钟";
            this.重试间隔分钟.Name = "重试间隔分钟";
            this.重试间隔分钟.ReadOnly = true;
            this.重试间隔分钟.Width = 75;
            // 
            // 最后执行时间
            // 
            this.最后执行时间.HeaderText = "最后执行时间";
            this.最后执行时间.Name = "最后执行时间";
            this.最后执行时间.ReadOnly = true;
            // 
            // mailfrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1445, 848);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mailfrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "任务管理";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.mailfrm_Load);
            this.Shown += new System.EventHandler(this.mailfrm_Shown);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 立即执行;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.ToolStripButton toolStripButton8;
        private System.Windows.Forms.ToolStripButton toolStripButton9;
        private System.Data.DataSet ds;
        private System.Windows.Forms.ToolStripButton toolStripButton10;
        private System.Windows.Forms.ToolStripButton toolStripButton11;
        private System.Windows.Forms.ToolStripMenuItem 日志查看ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 发送测试邮件ToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn SN;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 启用;
        private System.Windows.Forms.DataGridViewTextBoxColumn 排程名称;
        private System.Windows.Forms.DataGridViewTextBoxColumn 分;
        private System.Windows.Forms.DataGridViewTextBoxColumn 时;
        private System.Windows.Forms.DataGridViewTextBoxColumn 天;
        private System.Windows.Forms.DataGridViewTextBoxColumn 月;
        private System.Windows.Forms.DataGridViewTextBoxColumn 周;
        private System.Windows.Forms.DataGridViewTextBoxColumn 执行命令;
        private System.Windows.Forms.DataGridViewTextBoxColumn MailTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn MailCC;
        private System.Windows.Forms.DataGridViewTextBoxColumn MailBCC;
        private System.Windows.Forms.DataGridViewTextBoxColumn MailSubject;
        private System.Windows.Forms.DataGridViewTextBoxColumn MailBody;
        private System.Windows.Forms.DataGridViewTextBoxColumn 附件文件名;
        private System.Windows.Forms.DataGridViewCheckBoxColumn 邮件发送失败重试;
        private System.Windows.Forms.DataGridViewTextBoxColumn 失败重试次数;
        private System.Windows.Forms.DataGridViewTextBoxColumn 重试间隔分钟;
        private System.Windows.Forms.DataGridViewTextBoxColumn 最后执行时间;
    }
}