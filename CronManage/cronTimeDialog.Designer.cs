﻿namespace CronManage
{
    partial class cronTimeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.checkedListBox3 = new System.Windows.Forms.CheckedListBox();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.checkedListBox4 = new System.Windows.Forms.CheckedListBox();
			this.tabPage5 = new System.Windows.Forms.TabPage();
			this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.checkedListBox5 = new System.Windows.Forms.CheckedListBox();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.button11 = new System.Windows.Forms.Button();
			this.button12 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
			this.panelControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
			this.panelControl2.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
			this.tabPage3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
			this.tabPage4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
			this.tabPage5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// panelControl1
			// 
			this.panelControl1.Controls.Add(this.button2);
			this.panelControl1.Controls.Add(this.button1);
			this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelControl1.Location = new System.Drawing.Point(0, 380);
			this.panelControl1.Name = "panelControl1";
			this.panelControl1.Size = new System.Drawing.Size(516, 70);
			this.panelControl1.TabIndex = 0;
			// 
			// button2
			// 
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(309, 19);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "取消";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(152, 19);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "确定";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// panelControl2
			// 
			this.panelControl2.Controls.Add(this.tabControl1);
			this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelControl2.Location = new System.Drawing.Point(0, 0);
			this.panelControl2.Name = "panelControl2";
			this.panelControl2.Size = new System.Drawing.Size(516, 380);
			this.panelControl2.TabIndex = 1;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Controls.Add(this.tabPage5);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(2, 2);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(512, 376);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.button4);
			this.tabPage1.Controls.Add(this.button3);
			this.tabPage1.Controls.Add(this.textEdit1);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.checkedListBox1);
			this.tabPage1.Location = new System.Drawing.Point(4, 23);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(504, 349);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "分";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// textEdit1
			// 
			this.textEdit1.Location = new System.Drawing.Point(100, 243);
			this.textEdit1.Name = "textEdit1";
			this.textEdit1.Size = new System.Drawing.Size(58, 20);
			this.textEdit1.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(17, 246);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(77, 14);
			this.label2.TabIndex = 2;
			this.label2.Text = "重复执行(分)";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(40, 49);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(19, 14);
			this.label1.TabIndex = 1;
			this.label1.Text = "分";
			// 
			// checkedListBox1
			// 
			this.checkedListBox1.CheckOnClick = true;
			this.checkedListBox1.ColumnWidth = 40;
			this.checkedListBox1.FormattingEnabled = true;
			this.checkedListBox1.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16"});
			this.checkedListBox1.Location = new System.Drawing.Point(100, 49);
			this.checkedListBox1.MultiColumn = true;
			this.checkedListBox1.Name = "checkedListBox1";
			this.checkedListBox1.Size = new System.Drawing.Size(332, 174);
			this.checkedListBox1.TabIndex = 0;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.button5);
			this.tabPage2.Controls.Add(this.button6);
			this.tabPage2.Controls.Add(this.textEdit2);
			this.tabPage2.Controls.Add(this.label3);
			this.tabPage2.Controls.Add(this.label4);
			this.tabPage2.Controls.Add(this.checkedListBox2);
			this.tabPage2.Location = new System.Drawing.Point(4, 23);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(504, 349);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "时";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// textEdit2
			// 
			this.textEdit2.Location = new System.Drawing.Point(130, 254);
			this.textEdit2.Name = "textEdit2";
			this.textEdit2.Size = new System.Drawing.Size(58, 20);
			this.textEdit2.TabIndex = 7;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(35, 257);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(89, 14);
			this.label3.TabIndex = 6;
			this.label3.Text = "重复执行(小时)";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(71, 43);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(19, 14);
			this.label4.TabIndex = 5;
			this.label4.Text = "时";
			// 
			// checkedListBox2
			// 
			this.checkedListBox2.CheckOnClick = true;
			this.checkedListBox2.ColumnWidth = 50;
			this.checkedListBox2.FormattingEnabled = true;
			this.checkedListBox2.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16"});
			this.checkedListBox2.Location = new System.Drawing.Point(131, 43);
			this.checkedListBox2.MultiColumn = true;
			this.checkedListBox2.Name = "checkedListBox2";
			this.checkedListBox2.Size = new System.Drawing.Size(114, 208);
			this.checkedListBox2.TabIndex = 4;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.button7);
			this.tabPage3.Controls.Add(this.button8);
			this.tabPage3.Controls.Add(this.textEdit3);
			this.tabPage3.Controls.Add(this.label5);
			this.tabPage3.Controls.Add(this.label6);
			this.tabPage3.Controls.Add(this.checkedListBox3);
			this.tabPage3.Location = new System.Drawing.Point(4, 23);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(504, 349);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "天";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// textEdit3
			// 
			this.textEdit3.Location = new System.Drawing.Point(138, 233);
			this.textEdit3.Name = "textEdit3";
			this.textEdit3.Size = new System.Drawing.Size(58, 20);
			this.textEdit3.TabIndex = 11;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(43, 236);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(77, 14);
			this.label5.TabIndex = 10;
			this.label5.Text = "重复执行(天)";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(78, 39);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(19, 14);
			this.label6.TabIndex = 9;
			this.label6.Text = "天";
			// 
			// checkedListBox3
			// 
			this.checkedListBox3.CheckOnClick = true;
			this.checkedListBox3.ColumnWidth = 40;
			this.checkedListBox3.FormattingEnabled = true;
			this.checkedListBox3.Location = new System.Drawing.Point(138, 39);
			this.checkedListBox3.MultiColumn = true;
			this.checkedListBox3.Name = "checkedListBox3";
			this.checkedListBox3.Size = new System.Drawing.Size(222, 174);
			this.checkedListBox3.TabIndex = 8;
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.button9);
			this.tabPage4.Controls.Add(this.button10);
			this.tabPage4.Controls.Add(this.textEdit4);
			this.tabPage4.Controls.Add(this.label7);
			this.tabPage4.Controls.Add(this.label8);
			this.tabPage4.Controls.Add(this.checkedListBox4);
			this.tabPage4.Location = new System.Drawing.Point(4, 23);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(504, 349);
			this.tabPage4.TabIndex = 3;
			this.tabPage4.Text = "月";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// textEdit4
			// 
			this.textEdit4.Enabled = false;
			this.textEdit4.Location = new System.Drawing.Point(132, 272);
			this.textEdit4.Name = "textEdit4";
			this.textEdit4.Size = new System.Drawing.Size(58, 20);
			this.textEdit4.TabIndex = 15;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(37, 275);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(77, 14);
			this.label7.TabIndex = 14;
			this.label7.Text = "重复执行(月)";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(72, 48);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(19, 14);
			this.label8.TabIndex = 13;
			this.label8.Text = "月";
			// 
			// checkedListBox4
			// 
			this.checkedListBox4.CheckOnClick = true;
			this.checkedListBox4.ColumnWidth = 50;
			this.checkedListBox4.FormatString = "#月";
			this.checkedListBox4.FormattingEnabled = true;
			this.checkedListBox4.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
			this.checkedListBox4.Location = new System.Drawing.Point(132, 48);
			this.checkedListBox4.MultiColumn = true;
			this.checkedListBox4.Name = "checkedListBox4";
			this.checkedListBox4.Size = new System.Drawing.Size(58, 208);
			this.checkedListBox4.TabIndex = 12;
			// 
			// tabPage5
			// 
			this.tabPage5.Controls.Add(this.button11);
			this.tabPage5.Controls.Add(this.button12);
			this.tabPage5.Controls.Add(this.textEdit5);
			this.tabPage5.Controls.Add(this.label9);
			this.tabPage5.Controls.Add(this.label10);
			this.tabPage5.Controls.Add(this.checkedListBox5);
			this.tabPage5.Location = new System.Drawing.Point(4, 23);
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage5.Size = new System.Drawing.Size(504, 349);
			this.tabPage5.TabIndex = 4;
			this.tabPage5.Text = "周";
			this.tabPage5.UseVisualStyleBackColor = true;
			// 
			// textEdit5
			// 
			this.textEdit5.Enabled = false;
			this.textEdit5.Location = new System.Drawing.Point(126, 196);
			this.textEdit5.Name = "textEdit5";
			this.textEdit5.Size = new System.Drawing.Size(58, 20);
			this.textEdit5.TabIndex = 15;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(31, 199);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(77, 14);
			this.label9.TabIndex = 14;
			this.label9.Text = "重复执行(周)";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(66, 37);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(19, 14);
			this.label10.TabIndex = 13;
			this.label10.Text = "周";
			// 
			// checkedListBox5
			// 
			this.checkedListBox5.CheckOnClick = true;
			this.checkedListBox5.ColumnWidth = 40;
			this.checkedListBox5.FormattingEnabled = true;
			this.checkedListBox5.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
			this.checkedListBox5.Location = new System.Drawing.Point(126, 37);
			this.checkedListBox5.MultiColumn = true;
			this.checkedListBox5.Name = "checkedListBox5";
			this.checkedListBox5.Size = new System.Drawing.Size(133, 123);
			this.checkedListBox5.TabIndex = 12;
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(438, 49);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(41, 23);
			this.button3.TabIndex = 4;
			this.button3.Text = "全选";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(438, 78);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(41, 23);
			this.button4.TabIndex = 5;
			this.button4.Text = "反选";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(251, 72);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(41, 23);
			this.button5.TabIndex = 9;
			this.button5.Text = "反选";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(251, 43);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(41, 23);
			this.button6.TabIndex = 8;
			this.button6.Text = "全选";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(366, 68);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(41, 23);
			this.button7.TabIndex = 13;
			this.button7.Text = "反选";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(366, 39);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(41, 23);
			this.button8.TabIndex = 12;
			this.button8.Text = "全选";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(196, 77);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(41, 23);
			this.button9.TabIndex = 17;
			this.button9.Text = "反选";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.button9_Click);
			// 
			// button10
			// 
			this.button10.Location = new System.Drawing.Point(196, 48);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(41, 23);
			this.button10.TabIndex = 16;
			this.button10.Text = "全选";
			this.button10.UseVisualStyleBackColor = true;
			this.button10.Click += new System.EventHandler(this.button10_Click);
			// 
			// button11
			// 
			this.button11.Location = new System.Drawing.Point(265, 66);
			this.button11.Name = "button11";
			this.button11.Size = new System.Drawing.Size(41, 23);
			this.button11.TabIndex = 17;
			this.button11.Text = "反选";
			this.button11.UseVisualStyleBackColor = true;
			this.button11.Click += new System.EventHandler(this.button11_Click);
			// 
			// button12
			// 
			this.button12.Location = new System.Drawing.Point(265, 37);
			this.button12.Name = "button12";
			this.button12.Size = new System.Drawing.Size(41, 23);
			this.button12.TabIndex = 16;
			this.button12.Text = "全选";
			this.button12.UseVisualStyleBackColor = true;
			this.button12.Click += new System.EventHandler(this.button12_Click);
			// 
			// cronTimeDialog
			// 
			this.AcceptButton = this.button1;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.button2;
			this.ClientSize = new System.Drawing.Size(516, 450);
			this.Controls.Add(this.panelControl2);
			this.Controls.Add(this.panelControl1);
			this.Name = "cronTimeDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "定时字串生成器";
			this.Shown += new System.EventHandler(this.cronTimeDialog_Shown);
			((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
			this.panelControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
			this.panelControl2.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
			this.tabPage3.ResumeLayout(false);
			this.tabPage3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
			this.tabPage4.ResumeLayout(false);
			this.tabPage4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
			this.tabPage5.ResumeLayout(false);
			this.tabPage5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckedListBox checkedListBox3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckedListBox checkedListBox4;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckedListBox checkedListBox5;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Button button11;
		private System.Windows.Forms.Button button12;
	}
}