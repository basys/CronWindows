/// args
var target = Argument("target", "default");


/// build task
Task("build")
    .Does(() =>
{
    MSBuild("./Cron.sln", new MSBuildSettings{
        Verbosity = Verbosity.Minimal
    });
});

Task("Cron")
    .Does(() =>
{
    MSBuild("./Cron/Cron.csproj", new MSBuildSettings{
        Verbosity = Verbosity.Minimal
    });
});



Task("default")
    .IsDependentOn("build");


Task("clean")
    .Does(() =>
{
    CleanDirectories("./src/*/bin");
    CleanDirectories("./test/*/bin");
});

/// run task
RunTarget(target);