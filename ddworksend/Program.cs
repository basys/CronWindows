﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CronCommon;
using System.Data;
using System.Data.SqlClient;
using DingTalk.Api;
using DingTalk.Api.Request;
using DingTalk.Api.Response;


namespace ddworksend
{
    class Program
    {
        public static string ddsendworkinfo(string AccessToken, string UseridList, string msgtext)
        {
            string msg = null;
            Log.Writter($"发给到用户清单:{UseridList}");
            IDingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2");
            OapiMessageCorpconversationAsyncsendV2Request req = new OapiMessageCorpconversationAsyncsendV2Request();
            req.AgentId = 1900554480L;
            req.UseridList = UseridList;
            OapiMessageCorpconversationAsyncsendV2Request.MsgDomain obj1 = new OapiMessageCorpconversationAsyncsendV2Request.MsgDomain();
            obj1.Msgtype = "action_card";

            OapiMessageCorpconversationAsyncsendV2Request.ActionCardDomain obj6 = new OapiMessageCorpconversationAsyncsendV2Request.ActionCardDomain();
            obj6.SingleUrl = "http://116.6.133.110:20001/mes/ShengChanShenHe/ShengChanShenHeIndex.aspx";
            obj6.SingleTitle = "你有需要处理的单据";
            obj6.Markdown = msgtext;
            obj6.Title = "有需要审核的打稿工作单,请尽快处理";
            obj1.ActionCard = obj6;
            req.Msg_ = obj1;
            OapiMessageCorpconversationAsyncsendV2Response rsp = client.Execute(req, AccessToken);
            Log.Writter($"完成后返回信息:{rsp.Body}");
            Log.Writter($"完成后返回信息:{rsp.Errmsg}");
            if (rsp.Errmsg == "ok") return rsp.Body;
            else return msg;
        }

        static void Main(string[] args)
        {
            SqlConnection conn = DbConn.GetDbConnection();
            try
            {
                if (conn == null)
                {
                    Log.Writter("数据连接失败");
                    return;
                }
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter("exec dbo.钉钉打稿审核消息处理", conn);
                da.Fill(ds);
                //Console.WriteLine(ds.Tables.Count.ToString());

                if (ds.Tables[0].Rows.Count <= 0)
                {
                    Log.Writter("没有找到需要的发送数据记录");
                    return;
                }

                string access_token = "";

                IDingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/gettoken");

                OapiGettokenRequest req = new OapiGettokenRequest();
                req.Appkey = "dingxlfhzy83wtjxfiim";
                req.Appsecret = "ScFiZ4HSUpyJ29nD7GmrqQZwK7GeWUSJ1Ahpik4CZpYG7KAkdopfz7EC5vvH37PK";

                req.SetHttpMethod("GET");
                OapiGettokenResponse rsp = client.Execute(req, access_token);
                Log.Writter($"取得应用程门票:{access_token}");

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string ri = ddsendworkinfo(rsp.AccessToken, row["DingTalkUID"].ToString(), row["msgtext"].ToString());

                    if (ri != null)
                    {
                        if (row["BM"].ToString() == "QC")
                        {
                            SqlHelper.ExecuteSql($"update 钉钉打稿审核记录 set QC消息发送ID='{ri}' where QC可审核人员='{row["AID"].ToString()}'");
                        }
                        else
                        {
                            SqlHelper.ExecuteSql($"update 钉钉打稿审核记录 set 生产消息发送ID='{ri}' where 生产可审核人员='{row["AID"].ToString()}'");
                        }
                    }
                }

                conn.Close();
                conn.Dispose();
                Log.Writter("关闭数据连接");
                Log.Writter("====================================================================================");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                conn.Close();
                conn.Dispose();
                Log.Writter("错误信息:" + ex.Message);
            }
        }
    }
}
