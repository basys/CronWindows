﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using CronCommon;
using System.IO;


namespace cronsend
{
    class Program
    {

        static void Main(string[] args)
        {
            //参数解析
            CmdParameter cmd = new CmdParameter(Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), args);
            if (!cmd.CmdParameterCheck) return;

            Log.Writter("----------------------------------------------");

            SqlConnection conn = DbConn.GetDbConnection();
            try
            {

                if (conn == null)
                {
                    Log.Writter("数据连接失败");
                    return;
                }
                
                CronStruct cs = DbConn.GetCron(conn, args[0]);
                if (cs == null)
                {
                    Log.Writter("取得任务数据失败");
                    return;
                }
                Log.Writter(conn.ConnectionString);
                DbConn.CronStateUpdae(conn, args[0]);


                cs.cmd = cmd;
                Log.Writter("初始部分参数完成");

                cs.InitMailData(conn);
                Log.Writter("初始化附件或内容完成");

                if (cmd.NoSendMail)
                {
                    Log.Writter("有带不发送邮件的参数,此次执行不发邮件");
                    return;
                }

                if (cs.是否发送邮件 == false)
                {
                    Log.Writter("初始化邮件发问未达标,此次执行不发邮件");
                    return;
                }

                if (cs.发邮件分组类型 == 0)
                {
                    if (cs.cmd.SendTestMail) cs.TestSendMailSet();

                    Email email = new Email(cs);
                    if (email.Send())
                    {
                        if (cs.邮件发送失败重试)
                        {
                            string SendRetryFilename = $"{ Dir.GetCurrentDirectory()}\\SendRetry\\{args[0]}.txt";
                            if (File.Exists(SendRetryFilename))
                            {
                                File.Delete(SendRetryFilename);
                                Log.Writter("删除文件" + SendRetryFilename);
                            }
                        }
                        Log.Writter("邮件发送完成");
                    }
                    else
                    {
                        if (cs.邮件发送失败重试)
                        {
                            CronCommon.RetrySendMailRecord.CronIdRedord(cs);
                            Log.Writter("邮件发送失败,记录准备重试");
                        }
                    }
                }
                else
                {
                    Log.Writter($"发送邮件准备开始,邮件封数{cs.mr.MailCount.ToString()}");
                    Log.Writter(cs.cmd.SendTestMail.ToString());

                    for (int i = 0; i<cs.mr.MailCount; i++)
                    {
                        cs.SwitchMailArray(i);
                        if (cs.cmd.SendTestMail) cs.TestSendMailSet();
                        Log.Writter("切换邮件地址完成");

                        Email email = new Email(cs);

                        if (email.Send())
                        {
                            if (cs.邮件发送失败重试)
                            {
                                string SendRetryFilename = $"{ Dir.GetCurrentDirectory()}\\SendRetry\\{args[0]}.txt";
                                if (File.Exists(SendRetryFilename))
                                {
                                    File.Delete(SendRetryFilename);
                                    Log.Writter("删除文件" + SendRetryFilename);
                                }
                            }
                            Log.Writter("邮件发送完成");
                        }
                        else
                        {
                            if (cs.邮件发送失败重试)
                            {
                                CronCommon.RetrySendMailRecord.CronIdRedord(cs);
                                Log.Writter("邮件发送失败,记录准备重试");
                            }
                        }
                    }

                }


                conn.Close();
                conn.Dispose();
                Log.Writter("关闭数据连接");
                Log.Writter("====================================================================================");
            }
            catch (Exception ex)
            {                
                conn.Close();
                conn.Dispose();
                RetrySendMailRecord.CronIdRedord(args[0]);
                Log.Writter("错误信息:" + ex.Message);
            }
        }
    }
}
