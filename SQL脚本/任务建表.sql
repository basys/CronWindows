CREATE TABLE [dbo].[crontab] (
  [SN] varchar(7) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [启用] bit DEFAULT ((0)) NULL,
  [排程名称] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [分] varchar(20) COLLATE Chinese_PRC_CI_AS DEFAULT ('*') NULL,
  [时] varchar(20) COLLATE Chinese_PRC_CI_AS DEFAULT ('*') NULL,
  [天] varchar(20) COLLATE Chinese_PRC_CI_AS DEFAULT ('*') NULL,
  [月] varchar(20) COLLATE Chinese_PRC_CI_AS DEFAULT ('*') NULL,
  [周] varchar(20) COLLATE Chinese_PRC_CI_AS DEFAULT ('*') NULL,
  [执行命令] varchar(20) COLLATE Chinese_PRC_CI_AS DEFAULT ('cronsend.exe') NULL,
  [最后执行时间] datetime  NULL,
  [MailTO] varchar(1200) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [MailCC] varchar(1200) COLLATE Chinese_PRC_CI_AS  NULL,
  [MailBCC] varchar(1200) COLLATE Chinese_PRC_CI_AS  NULL,
  [MailSubject] varchar(150) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [MailBody] varchar(250) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [附件文件名] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [附件格式] varchar(5) COLLATE Chinese_PRC_CI_AS DEFAULT ('xls') NULL,
  [附件名时间] bit  NULL,
  [文件名后缀] varchar(20) COLLATE Chinese_PRC_CI_AS  NULL,
  [重试间隔分钟] int  NULL,
  [邮件发送失败重试] bit DEFAULT ((0)) NULL,
  [附件HTML内客] bit DEFAULT ((0)) NULL,
  [失败重试次数] int DEFAULT ((3)) NULL,
  [数据栏位格式化] varchar(500) NULL,
  [发送邮件类型] int DEFAULT ((0)) NULL,
  [发送钉钉消息] bit DEFAULT ((0)) NULL,
  [钉钉消息地址] NVARCHAR(50) NULL,
  CONSTRAINT [PK__crontab__32151C643C0C34F2] PRIMARY KEY CLUSTERED ([SN])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
)  
ON [PRIMARY]


CREATE TABLE [dbo].[cronExportFile] (
  [SN] varchar(10) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [序号] int  NOT NULL,
  [SheetName] varchar(50) COLLATE Chinese_PRC_CI_AS  NULL,
  [存储过程] varchar(50) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [参数1] varchar(150) COLLATE Chinese_PRC_CI_AS  NULL,
  [参数2] varchar(150) COLLATE Chinese_PRC_CI_AS  NULL,
  [参数3] varchar(150) COLLATE Chinese_PRC_CI_AS  NULL,
  [参数4] varchar(150) COLLATE Chinese_PRC_CI_AS  NULL,
  [参数5] varchar(150) COLLATE Chinese_PRC_CI_AS  NULL,
  CONSTRAINT [PK__cronExpo__83DAE582383BA40E] PRIMARY KEY CLUSTERED ([SN], [序号])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
)  
ON [PRIMARY]

CREATE TABLE [dbo].[cronhistory] (
  [ID] int  IDENTITY(1,1) NOT NULL,
  [SN] nvarchar(8) COLLATE Chinese_PRC_CI_AS  NULL,
  [附件] nvarchar(300) COLLATE Chinese_PRC_CI_AS  NULL,
  [发送时间] datetime DEFAULT (getdate()) NULL,
  [状态] nvarchar(10) COLLATE Chinese_PRC_CI_AS  NULL,
  CONSTRAINT [PK__cronhist__3214EC27250E636A] PRIMARY KEY CLUSTERED ([ID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
)  
ON [PRIMARY]


