﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FastReport;
using FastReport.Export.Pdf;
using CronCommon;
using System.Data.SqlClient;
using System.IO;


namespace testreport
{
    public partial class Form1 : Form
    {
        string connstr = "server=192.168.19.19;database=VietnamDB;uid=sa;pwd=admin9898;MultipleActiveResultSets=true;Min Pool Size=1";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);

            SqlDataAdapter ad  = new SqlDataAdapter("exec sp_yd_sales_new '20200928'", conn);
            DataSet ds=new DataSet();
            ad.Fill(ds);

            string templateFilename = $"{Dir.GetCurrentDirectory()}\\template\\越南DN金额.frx";
            Report r = new Report();
            r.Load(templateFilename);
            r.RegisterData(ds.Tables[0], "Table");
            r.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);

            SqlDataAdapter ad = new SqlDataAdapter("exec dbo.SP_YD_new '20200928'", conn);
            DataSet ds = new DataSet();
            ad.Fill(ds);

            string templateFilename = $"{Dir.GetCurrentDirectory()}\\template\\越南PO金额.frx";
            Report r = new Report();
            r.Load(templateFilename);
            r.RegisterData(ds.Tables[0], "Table");
            r.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);

            SqlDataAdapter ad = new SqlDataAdapter("exec sp_yd_sales '20200429'", conn);
            DataSet ds = new DataSet();
            ad.Fill(ds);

            string templateFilename = $"{Dir.GetCurrentDirectory()}\\template\\DN金额.frx";
            Report r = new Report();
            r.Load(templateFilename);
            r.RegisterData(ds.Tables[0], "Table");
            r.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);

            SqlDataAdapter ad = new SqlDataAdapter("exec dbo.SP_YD '20200429'", conn);
            DataSet ds = new DataSet();
            ad.Fill(ds);

            string templateFilename = $"{Dir.GetCurrentDirectory()}\\template\\PO金额.frx";
            Report r = new Report();
            r.Load(templateFilename);
            r.RegisterData(ds.Tables[0], "Table");
            r.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connstr);

            SqlDataAdapter ad = new SqlDataAdapter("select top 10 * from 业务订单", conn);
            DataSet ds = new DataSet();
            ad.Fill(ds);

           string temp= DbConn.DataTableToHTML(ds.Tables[0]);
            memoEdit1.Text = temp;
        }

        private void button6_Click(object sender, EventArgs e)
        {

            StrDecompose(1, "01");
        }

        private static List<string> decoder(string s)
        {
            List<string> s1 = new List<string>();
            List<string> s2 = new List<string>();
            List<string> ret = new List<string>();
            s1 = s.Split(',').ToList();
            foreach (var s3 in s1)
            {
                s2 = s3.Split('-').ToList();
                if (s2.Count > 1)
                {
                    for (int i = Convert.ToInt32(s2[0]); i <= Convert.ToInt32(s2[1]); i++)
                    {
                        ret.Add(i.ToString());
                    }
                }
                else
                {
                    ret.Add(s2[0].ToString());
                }

            }
            s1 = null;
            s2 = null;
            return ret;
        }

        private static bool StrDecompose(int q, string s)
        {
            Boolean result = false;

            string[] s1;
            if (s == "*")
            {
                result = true;
            }
            else if (s.IndexOf('/') >= 0)
            {
                s1 = s.Split('/');
                if (s1[0].IndexOf('-') >= 0 || s1[0].IndexOf(',') >= 0)
                {
                    if (decoder(s1[0].ToString()).IndexOf(q.ToString()) >= 0)
                        if (q % Convert.ToInt32(s1[1]) == 0) result = true;
                }
                else
                {
                    if (q % Convert.ToInt32(s1[1]) == 0) result = true;
                }
            }
            else
            {
                if (s.IndexOf('-') >= 0 || s.IndexOf(',') >= 0)
                {
                    if (decoder(s).IndexOf(q.ToString()) >= 0) result = true;
                }
                else
                {
                    if (q == Convert.ToInt32(s)) result = true;
                }
            }

            return result;

        }

        private void button7_Click(object sender, EventArgs e)
        {
            var files = Directory.GetFiles($".\\log", "*JOB002.log");

            var query = from e1 in files.AsEnumerable()
                        where(e1.Contains("JOB002"))
                        select e1;
            foreach (string filename in query)
            {
                //listBox1.Items.Add(filename);
             }
        }
    }
}
